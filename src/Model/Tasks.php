<?php
/**
 * Created by PhpStorm.
 * User: bonynight
 * Date: 21.11.19
 * Time: 13:09
 */

namespace App\Model;

class Tasks {
    private $tb_name = 'tasks';

    private $db;

    public function __construct (\PDO $db)
    {
        $this->db = $db;
    }

    public function getList(  $from = 0,  $limit = 10)
    {
        try {
            $sql = $this->db->prepare("SELECT * FROM $this->tb_name LIMIT $from, $limit");
            $sql->execute();
            return $sql->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\Throwable $e)
        {
            echo $e->getMessage();
        }
    }

    public function getTotalTasks()
    {
        $sql = $this->db->prepare("SELECT count(*) as total FROM $this->tb_name");
        $sql->execute();
        return $sql->fetch(\PDO::FETCH_ASSOC)['total'];
    }

    public function add($params)
    {
        try {
            $sql = $this->db->prepare("INSERT INTO  `$this->tb_name` (user_name, content, email) VALUES (:user_name, :content, :email);");
            $sql->bindValue(':user_name', $params['user_name'], \PDO::PARAM_STR);
            $sql->bindValue(':content',  $params['content'], \PDO::PARAM_STR);
            $sql->bindValue(':email', $params['email'], \PDO::PARAM_STR);
            $sql->execute();

            return true;

        } catch (\Throwable $e)
        {
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try {
            $sql = $this->db->prepare("DELETE FROM `$this->tb_name` WHERE id=(:id)");
            $sql->bindValue(':id', $id, \PDO::PARAM_STR);
            $sql->execute();
        } catch (\Throwable $e)
        {
            echo $e->getMessage();
        }

    }


} 