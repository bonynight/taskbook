<?php
/**
 * Created by PhpStorm.
 * User: bonynight
 * Date: 21.11.19
 * Time: 22:41
 */

namespace App\Libs;

class Session {

    private $session;

    public function __construct()
    {
        session_start();
        $this->session = $_SESSION;
    }

    public function set($key, $value)
    {
        if ($key) {
            $this->session[$key] = $value;
        }
        if (null === $value) {
            unset($this->session[$key]);
        }
        $_SESSION = $this->session;
    }

    public function get($key)
    {
        return $this->session[$key];
    }
} 