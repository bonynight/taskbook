<?php
/**
 * Created by PhpStorm.
 * User: bonynight
 * Date: 21.11.19
 * Time: 22:24
 */

namespace App\Controller;
use Smarty;
use App\Libs\Session as Session;


class Auth {
    /**
     * @var Smarty
     */
    private $template;

    private $session;

    private $login;
    private $password;

    public function __construct(Smarty $smarty, Session $session)
    {
        $this->template = $smarty;
        $this->session = $session;

        $this->login = 'admin';
        $this->password = '123';
    }

    public function showAuthForm()
    {
        if (!$this->session->get('admin'))
        {
            return $this->template->display('showAuthForm.tpl');
        } else {
            header("Location: /list");
        }
    }

    public function showAdminBar()
    {
        return $this->template->display('showAdminBar.tpl');
    }

    public function login()
    {
        if ($this->login == $_POST['login'] && $this->password = $_POST['password'])
        {
            $this->session->set('admin', true);
        }
        header("Location: /list");
    }


    public function logout()
    {
        $this->session->set('admin', null);
        header("Location: /list");
    }
}