<?php
namespace App\Controller;

use Smarty;
use App\Model\Tasks as ModelTask;
use App\Libs\Paginator;
use App\Libs\Session;

class Tasks {
    /**
     * @var Smarty
     */
    private $template;

    /**
     * @var ModelTask
     */
    private $modelTask;

    /**
     * @var Paginator
     */
    private $pagination;

    /**
     *
     */
    private $session;

    /**
     *
     */
    public function __construct (Smarty $smarty, ModelTask $model, Paginator $pagination, Session $session)
    {
        $this->template = $smarty;
        $this->modelTask = $model;
        $this->pagination = $pagination;
        $this->session = $session;
    }

    /**
     *
     */
    public function  showTasksList($page = null, $onpage = null)
    {
        $this->pagination->set_total($this->modelTask->getTotalTasks());
        $getLimit = $this->pagination->get_limit_raw();

        $taskList = $this->modelTask->getList(...array_values($getLimit));
        $this->template->assign('taskList', $taskList);
        $this->template->assign('session', $this->session);
        $this->template->assign('pagination', $this->pagination->page_links());
        return $this->template->display('showTasksList.tpl');
    }

    /**
     *
     */
    public function showTasksAdd()
    {
        $this->template->assign('session', $this->session);
        return $this->template->display('showTasksAdd.tpl');
    }

    public function addTask()
    {
        //надо отвалидировать POST
        $this->modelTask->add($_POST);
        header("Location: /list");
    }

    public function deleteTask($id = null)
    {
        if (!$this->session->get('admin')) {
            header("Location: /list");
            return;
        }
        if (filter_var($id, FILTER_VALIDATE_INT))
        {
            $this->modelTask->delete($id);
            header("Location: /list");
        } else
        {
            echo 'Не валидный ид';
        }
    }


}