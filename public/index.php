<?php
require_once '../vendor/autoload.php';

use Pimple\Container;
use App\Libs\Paginator;
use App\Libs\Session;

$container = new Container();
$container['App\Controller\Tasks'] = function(Pimple\Container $container) {
    return new App\Controller\Tasks($container['Smarty'], $container['App\Model\Tasks'], $container['Pagination'], $container['Session']);
};
$container['App\Controller\Auth'] = function(Pimple\Container $container) {
    return new App\Controller\Auth($container['Smarty'], $container['Session']);
};
$container['App\Model\Tasks'] = function (Pimple\Container $container) {
    return new App\Model\Tasks($container['DB']);
};
$container['Session'] = function() {
    return new App\Libs\Session();
};

$container['Smarty'] = function() {
    $smarty = new Smarty();
    $smarty->template_dir = '../templates/';
    $smarty->compile_dir = '../var/cache/templates_c/';
    return $smarty;
};

$container['DB'] = function() {
    try {
        $dbConnection = new \PDO("sqlite:" . "/var/www/data/database.sqlite");
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbConnection;
    } catch (PDOException $e) {
        echo 'Подключение не удалось: ' . $e->getMessage();
    }
};

/**
 * @return Paginator
 */
$container['Pagination'] = function() {
    $p = new Paginator('5','page');
    $p->set_withLinkInCurrentLi(true);
    return $p;
};


$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/list', 'App\Controller\Tasks::showTasksList');
    $r->addRoute('GET', '/', 'App\Controller\Tasks::showTasksList');
    $r->addRoute('GET', '/list?page={page:\d+}', 'App\Controller\Tasks::showTasksList');
    $r->addRoute('GET', '/remove', 'App\Controller\Tasks::deleteTask');
    $r->addRoute('GET', '/remove?id={id:\d+}', 'App\Controller\Tasks::deleteTask');
    $r->addRoute('GET', '/add', 'App\Controller\Tasks::showTasksAdd');
    $r->addRoute('GET', '/admin', 'App\Controller\Auth::showAuthForm');
    $r->addRoute('POST', '/addTask', 'App\Controller\Tasks::addTask');
    $r->addRoute('POST', '/login', 'App\Controller\Auth::login');
    $r->addRoute('GET', '/logout', 'App\Controller\Auth::logout');


});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $uriStripped= $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uriStripped, '?')) {
    $uriStripped = substr($uriStripped, 0, $pos);
}
$uriStripped = rawurldecode($uriStripped);

$routeInfo = $dispatcher->dispatch($httpMethod, $uriStripped);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo '404 Not Found';
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        echo '405 Method Not Allowed';
        // ... 405 Method Not Allowed
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        $vars = array_merge($_GET, $vars);
        list($class, $method) = explode('::', $handler);
        $container[$class]->$method(...array_values($vars));
        break;
}


