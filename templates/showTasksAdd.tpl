{include file='header.tpl'}
{include file='showAdminBar.tpl'}
{include file='navBar.tpl'}

<form method="post" action="/addTask">
    <div class="form-group">
        <label for="exampleInputName">Имя пользователя*</label>
        <input type="text" required class="form-control" id="exampleInputName" name="user_name">
    </div>
    <div class="form-group">
        <label for="exampleInputContent">Задача*</label>
        <textarea class="form-control" required id="exampleInputContent" name="content"></textarea>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email*</label>
        <input type="email" required class="form-control" id="exampleInputEmail1" name="email">
    </div>
    <button type="submit" class="btn btn-primary">Добавить</button>
</form>

{include file='footer.tpl'}