{assign var="is_admin" value=$session->get('admin') scope=root}

{if $is_admin}
    Администраторская сессия <a href="/logout">выйти</a>
{else}
    Администраторская сессия <a href="/admin">войти</a>
{/if}
