{include file='header.tpl'}
{include file='showAdminBar.tpl'}
{include file='navBar.tpl'}


<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Имя</th>
        <th scope="col">Задача</th>
        <th scope="col">Email</th>
        {if $is_admin}
            <th scope="col"></th>
        {/if}
    </tr>
    </thead>
    <tbody>
    {foreach from=$taskList item=task}
        <tr>
            <th scope="row">{$task.id}</th>
            <td>{$task.user_name}</td>
            <td>{$task.content}</td>
            <td>{$task.email}</td>
            {if $is_admin}
            <td>
                <a href="/remove?id={$task.id}">
                    <button type="button" class="close" aria-label="Close" action="***">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </a>
            </td>
            {/if}
        </tr>
    {/foreach}
    </tbody>
</table>
<nav aria-label="Page navigation example">
    {$pagination}
</nav>

{include file='footer.tpl'}