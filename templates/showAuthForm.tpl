{include file='header.tpl'}

<form method="post" action="/login">
    <div class="form-group">
        <label for="exampleInputLogin">Логин</label>
        <input type="text" required class="form-control" id="exampleInputLogin" name="login">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Пароль</label>
        <input type="password" required class="form-control" id="exampleInputPassword1" name="password">
    </div>
    <button type="submit" class="btn btn-primary">Вход</button>
</form>

{include file='footer.tpl'}